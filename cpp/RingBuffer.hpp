
#pragma once

#include <cstdint>
#include <functional>

#include <Flat/Core/Ref.hpp>




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT RingBuffer
{
public:
	typedef std::function<bool(const uint8_t*, int)> Visitor;

	RingBuffer();
	~RingBuffer();

	void openForRead();
	void openForWrite();

	void push(const uint8_t * data, int size);
	void pop(int size);
	void clear();

	int bytesAvailable() const;
	int size() const;

	int pos() const;
	bool seek(int pos);

	int read(uint8_t * data, int maxLen);
	int write(const uint8_t * data, int maxLen);

	void visit(const Visitor & visitor);

private:
	struct D;

	Ref<D> d_;
};




}}
