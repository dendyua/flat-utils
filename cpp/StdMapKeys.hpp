
#pragma once

#include <string>
#include <string_view>
#include <unordered_map>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




struct StringViewKey {
	std::string_view sv;
};

inline bool operator<(const std::string & s, const StringViewKey & k) noexcept
{ return std::string_view(s) < k.sv; }

inline bool operator<(const StringViewKey & k, const std::string & s) noexcept
{ return k.sv < std::string_view(s); }

inline bool operator==(const std::string & s, const StringViewKey & k) noexcept
{ return s == k.sv; }

inline bool operator==(const StringViewKey & k, const std::string & s) noexcept
{ return s == k.sv; }




template <typename K, typename V, typename SK>
inline typename std::unordered_map<K, V>::iterator findUnorderedMapValue(
		std::unordered_map<K, V> & m, const SK & key) noexcept
{
	typedef typename std::unordered_map<K, V>::size_type BucketIndex;
	typedef typename std::unordered_map<K, V>::iterator Iterator;

	const int bucketCount = m.bucket_count();

	const auto hashFunc = std::hash<SK>();
	const auto hash = hashFunc(key);
	const BucketIndex bucketIndex = hash % bucketCount;

	const auto it = [bucketIndex, &m, &key] () -> Iterator {
		const auto end = m.end(bucketIndex);
		for (auto lit = m.begin(bucketIndex); lit != end; ++lit) {
			if (lit->first == key) {
				const auto it = m.find(lit->first);
				return it;
			}
		}
		return m.end();
	}();

	const K referenceKey = K(key);
	const auto referenceHashFunc = m.hash_function();
	const auto referenceHash = referenceHashFunc(referenceKey);
	const auto referenceIt = m.find(referenceKey);
	FLAT_ASSERT(hash == referenceHash);
	FLAT_ASSERT(it == referenceIt);

	return it;
}


template <typename K, typename V, typename SK>
inline typename std::unordered_map<K, V>::const_iterator findUnorderedMapValue(
		const std::unordered_map<K, V> & m, const SK & key) noexcept
{
	typedef typename std::unordered_map<K, V>::size_type BucketIndex;
	typedef typename std::unordered_map<K, V>::const_iterator Iterator;

	const int bucketCount = m.bucket_count();

	const auto hashFunc = std::hash<SK>();
	const auto hash = hashFunc(key);
	const BucketIndex bucketIndex = hash % bucketCount;

	const auto it = [bucketIndex, &m, &key] () -> Iterator {
		const auto end = m.end(bucketIndex);
		for (auto lit = m.begin(bucketIndex); lit != end; ++lit) {
			if (lit->first == key) {
				const auto it = m.find(lit->first);
				return it;
			}
		}
		return m.end();
	}();

	const K referenceKey = K(key);
	const auto referenceHashFunc = m.hash_function();
	const auto referenceHash = referenceHashFunc(referenceKey);
	const auto referenceIt = m.find(referenceKey);
	FLAT_ASSERT(hash == referenceHash);
	FLAT_ASSERT(it == referenceIt);

	return it;
}


template <typename K, typename V, typename SK>
inline V & getUnorderedMapValue(std::unordered_map<K, V> & m, const SK & key) noexcept
{
	typedef typename std::unordered_map<K, V>::size_type BucketIndex;

	const int bucketCount = m.bucket_count();

	const auto hashFunc = std::hash<SK>();
	const auto hash = hashFunc(key);
	const BucketIndex bucketIndex = hash % bucketCount;

	const auto end = m.end(bucketIndex);
	for (auto lit = m.begin(bucketIndex); lit != end; ++lit) {
		if (lit->first == key) {
			return lit->second;
		}
	}

	return m[K(key)];
}




}}
