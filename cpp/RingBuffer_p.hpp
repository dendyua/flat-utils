
#pragma once

#include <cstdint>
#include <functional>
#include <list>
#include <memory>
#include <vector>




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT RingBufferPrivate
{
public:
	typedef std::shared_ptr<std::vector<uint8_t>> Buffer;
	typedef std::function<bool(const uint8_t*, int)> Visitor;

	RingBufferPrivate();

	bool seek(int pos);

	int pos() const noexcept { return pos_; }
	int size() const noexcept { return size_; }

	int readData(uint8_t * data, int maxLen);
	int writeData(const uint8_t * data, int maxLen);

	void appendBuffer(int size);

	void push(const uint8_t * data, int size);
	void push(const Buffer & buffer);
	void pop(int size);
	void clear();

	void visit(const Visitor & visitor); // iterate over the ring buffers

private:
	std::vector<Buffer> cache_; // unused buffers
	std::vector<Buffer> ring_;  // buffer chain
	int pos_ = 0;               // current read pos
	int size_ = 0;              // total size
	int ringBufferNumber_ = -1; // current buffer for read pos
	int ringBufferPos_;         // current offset in buffer for read pos
	int offset_ = 0;            // offset in first chain buffer
};




}}
