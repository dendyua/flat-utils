
#pragma once

#include <array>
#include <cstdint>




namespace Flat {
namespace Utils {




template <typename T, int MaxChunks>
class SpeedCounter
{
public:
	SpeedCounter();

	double value() const;

	int timeout() const;
	void setTimeout(int64_t timeoutUsecs);

	void reset();

	void hit(T samples, int64_t usecs, int64_t timeoutUsecs);

private:
	struct Chunk { T samples; int64_t usecs; int64_t timeoutUsecs; };

	void _pushChunk(const Chunk & chunk);
	void _popChunk();
	void _calculateSpeed();

	int64_t timeout_ = 0;
	std::array<Chunk, MaxChunks> chunks_;
	int chunkPos_ = 0;
	int chunkCount_ = 0;
	T currentSamples_ = T(0);
	int64_t currentUsecs_ = 0;
	int64_t currentTimeoutUsecs_ = 0;
	double value_ = 0.0;
};




template <typename T, int MaxChunks>
inline SpeedCounter<T, MaxChunks>::SpeedCounter()
{
}


template <typename T, int MaxChunks>
inline double SpeedCounter<T, MaxChunks>::value() const
{
	return value_;
}


template <typename T, int MaxChunks>
inline int SpeedCounter<T, MaxChunks>::timeout() const
{
	return timeout_;
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::setTimeout(const int64_t timeoutUsecs)
{
	if (timeout_ == timeoutUsecs) return;

	timeout_ = timeoutUsecs;

	_calculateSpeed();
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::reset()
{
	currentSamples_ = T(0);
	currentUsecs_ = 0;
	currentTimeoutUsecs_ = 0;
	value_ = 0;
	chunkPos_ = 0;
	chunkCount_ = 0;
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::hit(const T samples, const int64_t usecs,
		const int64_t timeoutUsecs)
{
	if (chunkCount_ == MaxChunks) _popChunk();

	Chunk chunk;
	chunk.samples = samples;
	chunk.usecs = usecs;
	chunk.timeoutUsecs = timeoutUsecs == -1 ? usecs : timeoutUsecs;
	_pushChunk(chunk);

	_calculateSpeed();
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::_pushChunk(const Chunk & chunk)
{
	const int pos = (chunkPos_ + chunkCount_) % MaxChunks;
	chunks_[pos] = chunk;

	currentSamples_ += chunk.samples;
	currentUsecs_ += chunk.usecs;
	currentTimeoutUsecs_ += chunk.timeoutUsecs;

	chunkCount_++;
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::_popChunk()
{
	const Chunk & chunk = chunks_[chunkPos_];

	currentSamples_--;
	currentUsecs_ -= chunk.usecs;
	currentTimeoutUsecs_ -= chunk.timeoutUsecs;

	chunkCount_--;
	chunkPos_++;
	if (chunkPos_ == MaxChunks) chunkPos_ = 0;
}


template <typename T, int MaxChunks>
inline void SpeedCounter<T, MaxChunks>::_calculateSpeed()
{
	if (chunkCount_ == 0) return;

	// clear extra chunks
	while (currentTimeoutUsecs_ > timeout_) {
		// can't be less then one chunk
		if (chunkCount_ == 1) break;
		_popChunk();
	}

	if (currentUsecs_ == 0) {
		// speed is infinite
		value_ = 999999.0;
	} else {
		value_ = static_cast<double>(currentSamples_) * 1000000.0 / currentUsecs_;
	}
}




}}
