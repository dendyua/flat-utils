
#include "BufferStream.hpp"

#include "RingBuffer.hpp"
#include "Stream_p.hpp"




namespace Flat {
namespace Utils {




struct BufferStream::D : Stream::D
{
	RingBuffer b;
};




inline const BufferStream::D & BufferStream::_d() const noexcept
{
	return static_cast<const D&>(d());
}


inline BufferStream::D & BufferStream::_d() noexcept
{
	return static_cast<D&>(d());
}


BufferStream::BufferStream() :
	Stream(*new D)
{
}


BufferStream::~BufferStream()
{
}


void BufferStream::clear()
{
	_d().b.clear();
}


void BufferStream::visit(const Visitor & visitor)
{
	_d().b.visit(visitor);
}


int BufferStream::size() const
{
	return _d().b.size();
}


int BufferStream::pos() const
{
	return _d().b.pos();
}


void BufferStream::seek(const int pos)
{
	_d().b.seek(pos);
}


int BufferStream::bytesAvailable() const
{
	auto & b = _d().b;
	return b.size() - b.pos();
}


int BufferStream::readData(uint8_t * const data, const int size)
{
	return _d().b.read(data, size);
}


void BufferStream::commit(const int pos)
{
	auto & b = _d().b;
	b.pop(pos);
}


int BufferStream::writeData(const uint8_t * const data, const int size)
{
	return _d().b.write(data, size);
}




}}
