
#include "RingBuffer.hpp"

#include "RingBuffer_p.hpp"




namespace Flat {
namespace Utils {




struct RingBuffer::D : public RingBufferPrivate {};




RingBuffer::RingBuffer() :
	d_(*new D())
{
}


RingBuffer::~RingBuffer()
{
}


void RingBuffer::push(const uint8_t * const data, const int size)
{
	d_->push(data, size);
}


void RingBuffer::pop(const int size)
{
	d_->pop(size);
}


void RingBuffer::clear()
{
	d_->clear();
}


void RingBuffer::openForRead()
{
	d_->seek(0);
}


void RingBuffer::openForWrite()
{
}


int RingBuffer::bytesAvailable() const
{
	return d_->size() - d_->pos();
}


int RingBuffer::size() const
{
	return d_->size();
}


int RingBuffer::pos() const
{
	return d_->pos();
}


bool RingBuffer::seek(const int pos)
{
	return d_->seek(pos);
}


int RingBuffer::read(uint8_t * const data, const int maxLen)
{
	return d_->readData(data, maxLen);
}


int RingBuffer::write(const uint8_t * const data, const int maxLen)
{
	return d_->writeData(data, maxLen);
}


void RingBuffer::visit(const Visitor & visitor)
{
	d_->visit(visitor);
}




}}
