
#pragma once

#include "Stream.hpp"




namespace Flat {
namespace Utils {




struct Stream::D
{
	Stream::ReaderCallbacks * readerCallbacks = nullptr;
	Stream::WriterCallbacks * writerCallbacks = nullptr;

	bool readerLocked = false;
	int posRead;

	bool writerLocked = false;
	bool hasBytesWritten = false;

	virtual ~D() = default;
};




}}
