
#include "IdGenerator.hpp"

#include <algorithm>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




static const int kDefaultEnlargeSize = 16;




/**
 * \class IdGenerator
 *
 * \brief The IdGenerator class manages unique identifiers.
 *
 * All variables needs a unique identifiers that those variable can be referenced to.
 * Sometimes pointers are suitable, because in current process space pointers are unique.
 * Sometimes string variables and indexes are unique too.
 * IdGenerator class always returns a unique identifier from the given range for a constant time O(1).
 *
 * For example you have some items in hash and want to reference them by unique numbers:
 *
 * \code
 * Hash<int,Item> items;
 * IdGenerator generator;
 * ...
 * int createItem()
 * {
 *     int id = generator.take(); // generates a unique id and marks it as used
 *     items[id] = Item();
 *     return id;
 * }
 * \endcode
 *
 * Later when this item removed from the hash:
 *
 * \code
 * void removeItem(int id)
 * {
 *     generator.free(id); // marks the given id as unused
 *     items.remove(id);
 * }
 * \endcode
 */

/**
 * Constructs generator instance with the maximum identifiers to take to be the given \a limit.
 * Passing \c -1 as \a limit value constructs generator with unlimited number identifiers to take.
 */

IdGenerator::IdGenerator(const int limit) noexcept :
	limit_(limit)
{
	FLAT_ASSERT(limit_ >= -1);

	freeCount_ = 0;
	firstFree_ = kInvalidId;
	lastFree_ = kInvalidId;

	firstTaken_ = kInvalidId;
	lastTaken_ = kInvalidId;

	ids_.reserve(limit == -1 ? kDefaultEnlargeSize : std::min(limit, kDefaultEnlargeSize));
}


inline void IdGenerator::_makeIdTaken(const int id, IdInfo * const idp) noexcept
{
	IdInfo & currentId = idp[id];

	currentId.isFree = false;

	currentId.previous = lastTaken_;
	if (lastTaken_ != kInvalidId) {
		idp[lastTaken_].next = id;
	} else {
		firstTaken_ = id;
	}

	lastTaken_ = id;
	currentId.next = kInvalidId;
}


IdGenerator::Id IdGenerator::takeUnique() noexcept
{
	return Id(IdData(*this));
}


IdGenerator::Id IdGenerator::adapt(const int id) noexcept
{
	FLAT_ASSERT(isTaken(id));
	return Id(IdData(*this, id));
}


/**
 * Returns a unique identifier and marks it as used so it will never be returned again unless been freed with free().
 * Returns \c 0 if no ids limit was reached.
 *
 * \sa free(), reserve(), limit()
 */

int IdGenerator::take() noexcept
{
	int id = kInvalidId;
	IdInfo * idp = nullptr;

	if (freeCount_ == 0) {
		FLAT_ASSERT(firstFree_ == kInvalidId);
		FLAT_ASSERT(lastFree_ == kInvalidId);

		id = ids_.size();

		if ( limit_ != -1 && id == limit_ ) {
			// no free ids left
			return kInvalidId;
		}

		if ( id >= static_cast<int>(ids_.capacity()) ) {
			ids_.reserve(limit_ == -1 ? id + kDefaultEnlargeSize :
					std::min(limit_, static_cast<int>(ids_.capacity()) + kDefaultEnlargeSize));
		}

		ids_.insert(ids_.end(), IdInfo());
		idp = ids_.data();
	} else {
		FLAT_ASSERT(firstFree_ != kInvalidId);
		FLAT_ASSERT(lastFree_ != kInvalidId);

		freeCount_--;

		idp = ids_.data();

		// take free id from start
		id = firstFree_;

		IdInfo & currentId = idp[id];
		FLAT_ASSERT(currentId.isFree);

		firstFree_ = currentId.next;
		if ( firstFree_ != kInvalidId ) {
			FLAT_ASSERT(freeCount_ > 0);
			FLAT_ASSERT(id != lastFree_);

			IdInfo & firstId = idp[firstFree_];
			firstId.previous = kInvalidId;
		} else {
			FLAT_ASSERT(freeCount_ == 0);
			FLAT_ASSERT(id == lastFree_);

			lastFree_ = kInvalidId;
		}
	}

	_makeIdTaken(id, idp);

	return id;
}


/**
 * Marks previously taken \a id identifier as unused so you can reuse it later.
 * You cannot free an identifier that has not been dedicated with take() earlier.
 *
 * \sa take()
 */

void IdGenerator::free(const int id) noexcept
{
	FLAT_ASSERT(id >= 0 && id < static_cast<int>(ids_.size()));

	IdInfo * idp = ids_.data();

	IdInfo & currentId = idp[id];
	FLAT_ASSERT(!currentId.isFree);
	currentId.isFree = true;

	if (currentId.previous != kInvalidId) {
		idp[currentId.previous].next = currentId.next;
	}
	if (currentId.next != kInvalidId) {
		idp[currentId.next].previous = currentId.previous;
	}
	if (id == firstTaken_) {
		firstTaken_ = currentId.next;
	}
	if (id == lastTaken_) {
		lastTaken_ = currentId.previous;
	}

	// put free id to begin
	if (firstFree_ != kInvalidId) {
		FLAT_ASSERT(freeCount_ != 0);
		FLAT_ASSERT(lastFree_ != kInvalidId);

		IdInfo & firstId = idp[firstFree_];
		firstId.previous = id;
		currentId.next = firstFree_;
		currentId.previous = kInvalidId;
		firstFree_ = id;
	} else {
		FLAT_ASSERT(freeCount_ == 0);
		FLAT_ASSERT(lastFree_ == kInvalidId);

		currentId.previous = kInvalidId;
		currentId.next = kInvalidId;
		firstFree_ = id;
		lastFree_ = id;
	}

	freeCount_++;
}


void IdGenerator::clear() noexcept
{
	while (firstTaken_ != kInvalidId) {
		free(firstTaken_);
	}
}


/**
 * Marks identifier with concrete \a id as used so it will never be returned again unless freed with free().
 * Use this method instead of take() if you know that \a id is currently unused.
 * For example when you saved unique identifiers earlier and want to reuse them in clean generator.
 * Reserving already taken id or id out of limit range is prohibited.
 *
 * \sa take(), free(), limit()
 */

void IdGenerator::reserve(const int id) noexcept
{
	FLAT_ASSERT(id >= 0);
	FLAT_ASSERT(limit_ == -1 || id < limit_);

	IdInfo * idp = nullptr;

	if (id >= static_cast<int>(ids_.size())) {
		if (id >= static_cast<int>(ids_.capacity())) {
			ids_.reserve(limit_ == -1 ? id + kDefaultEnlargeSize :
					std::min(limit_, static_cast<int>(ids_.capacity()) + kDefaultEnlargeSize));
		}

		const int firstId = ids_.size();
		ids_.insert(ids_.end(), id - firstId + 1, IdInfo());

		idp = ids_.data();
		for (int i = firstId; i <= id; ++i) {
			idp[i].isFree = true;
			idp[i].previous = i - 1;
			idp[i].next = i + 1;
		}

		// link first new id with the last previous
		idp[firstId].previous = lastFree_;
		if (lastFree_ != kInvalidId) {
			idp[lastFree_].next = firstId;
		} else {
			firstFree_ = firstId;
		}

		lastFree_ = id;
		idp[lastFree_].next = kInvalidId;

		freeCount_ += id + 1 - firstId;
	}

	freeCount_--;

	if (!idp) {
		idp = ids_.data();
	}

	IdInfo & currentId = idp[id];
	FLAT_ASSERT(currentId.isFree) << "IdGenerator::reserve() "
			"Attempt to reserve already taken id.";

	if (currentId.previous != kInvalidId) {
		idp[currentId.previous].next = currentId.next;
	}

	if (currentId.next != kInvalidId) {
		idp[currentId.next].previous = currentId.previous;
	}

	if (id == firstFree_) {
		firstFree_ = currentId.next;
	}

	if (id == lastFree_) {
		lastFree_ = currentId.previous;
	}

	_makeIdTaken(id, idp);

	FLAT_ASSERT(
			(freeCount_ == 0 && firstFree_ == kInvalidId && lastFree_ == kInvalidId) ||
			(freeCount_ != 0 && firstFree_ != kInvalidId && lastFree_ != kInvalidId));
}


/**
 * Returns \c true whether the given \a id was not taken.
 * Returns \c false otherwise.
 */

bool IdGenerator::isFree(const int id) const noexcept
{
	FLAT_ASSERT(id >= 0);
	FLAT_ASSERT(limit_ == -1 || id < limit_);

	if (id >= static_cast<int>(ids_.size())) {
		return true;
	}

	return ids_.at(id).isFree;
}


IdGenerator::iterator_t IdGenerator::erase(const iterator_t & it) noexcept
{
	iterator_t next(*this, ids_.at(it.cur).next);
	free(it.cur);
	return next;
}




/**
 * \class IdGenerator::Iterator
 *
 * \brief The IdGeneratorIterator class iterator over identifiers of IdGenerator.
 *
 * Iterator allows to quickly iterate over all taken identifiers of IdGenerator instance.
 *
 * Example:
 *
 * \code
 * IdGenerator generator;
 * generator.take(); // returns 0
 * generator.take(); // returns 1
 * ...
 * for (IdGeneratorIterator it(generator); it.hasNext();)
 *     debug() << "id =" << it.next();
 *
 * // produces:
 * // id = 0
 * // id = 1
 * \endcode
 */


/**
 * Constructs iterator that will iterate on identifiers of \a idGenerator.
 */



/**
 * Returns \c true if iterator not reached end, otherwise returns \c false.
 *
 * \sa next()
 */



/**
 * Advances iterator and returns current id.
 * Advancing beyond the end is not allowed.
 *
 * \sa hasNext()
 */





}}
