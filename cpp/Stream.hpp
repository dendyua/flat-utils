
#pragma once

#include <memory>

#include <Flat/Core/Ref.hpp>




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT Stream
{
public:
	struct IncompleteError {};
	struct WriteError {};

	struct WriterIf;
	struct ReaderIf;

	template <typename T> struct EnumTraits {
	};

	template <typename T> struct TypeTraits {
		inline static constexpr bool valid = false;
	};

	template <typename T> struct Serializer {
		static void write(WriterIf & w, const T & value);
		static void read(ReaderIf & r, T & value);
	};

	struct WriterCallbacks {
		virtual void ready() = 0;
	};

	struct ReaderCallbacks {
		virtual void ready() = 0;
	};

	struct FLAT_UTILS_EXPORT WriterIf {
		int size() const;
		void writeData(const uint8_t * data, int size);

		template <typename T> void write(const T & v);
	};
	struct FLAT_UTILS_EXPORT WriterDeleter { void operator()(WriterIf *); };
	typedef std::unique_ptr<WriterIf, WriterDeleter> Writer;

	struct FLAT_UTILS_EXPORT ReaderIf {
		int pos() const;
		int bytesAvailable() const;
		void readData(uint8_t * data, int size);
		void commit();

		template <typename T> T read();
		template <typename T> void read(T & value);
	};
	struct FLAT_UTILS_EXPORT ReaderDeleter { void operator()(ReaderIf *); };
	typedef std::unique_ptr<ReaderIf, ReaderDeleter> Reader;

	Stream();
	virtual ~Stream();

	void setReaderCallbacks(ReaderCallbacks * cb);
	void setWriterCallbacks(WriterCallbacks * cb);

	Writer lockForWrite();
	Reader lockForRead();

protected:
	struct D;

	template <typename T, typename Enable = void> struct _EnumTraitsWrapper {
		typedef T I;
	};

	template <typename T> struct _SerializerWrapper {
		static void write(WriterIf & w, const T & value);
		static void read(ReaderIf & r, T & value);
	};

	virtual int size() const = 0;
	virtual int pos() const = 0;
	virtual void seek(int pos) = 0;
	virtual int bytesAvailable() const = 0;
	virtual int readData(uint8_t * data, int size) = 0;
	virtual void commit(int pos) = 0;
	virtual int writeData(const uint8_t * data, int size) = 0;

	Stream(D & d);

	const D & d() const noexcept { return *d_; }
	D & d() noexcept { return *d_; }

private:
	Ref<D> d_;
};




template <typename T> struct Stream::_EnumTraitsWrapper<T,
		std::enable_if_t<!Stream::TypeTraits<T>::valid && std::is_enum_v<T>>> {
	typedef typename EnumTraits<T>::I I;
};




template <typename T>
inline void Stream::_SerializerWrapper<T>::write(Stream::WriterIf & w, const T & value)
{
	typedef typename _EnumTraitsWrapper<T>::I U;
	Serializer<U>::write(w, static_cast<U>(value));
}


template <typename T>
inline void Stream::_SerializerWrapper<T>::read(Stream::ReaderIf & r, T & value)
{
	typedef typename _EnumTraitsWrapper<T>::I U;
	if constexpr (std::is_same_v<T, U>) {
		Serializer<T>::read(r, value);
	} else {
		U v;
		Serializer<U>::read(r, v);
		value = static_cast<T>(v);
	}
}




template <typename T>
inline void Stream::WriterIf::write(const T & v) { _SerializerWrapper<T>::write(*this, v); }

template <typename T>
inline T Stream::ReaderIf::read() { T v; _SerializerWrapper<T>::read(*this, v); return v; }

template <typename T>
inline void Stream::ReaderIf::read(T & v) { _SerializerWrapper<T>::read(*this, v); }




template <>
inline void Stream::Serializer<int32_t>::write(Stream::WriterIf & w, const int32_t & v)
{ w.writeData(reinterpret_cast<const uint8_t*>(&v), sizeof(v)); }

template <>
inline void Stream::Serializer<int32_t>::read(Stream::ReaderIf & r, int32_t & v)
{ r.readData(reinterpret_cast<uint8_t*>(&v), sizeof(v)); }


template <>
inline void Stream::Serializer<uint32_t>::write(Stream::WriterIf & w, const uint32_t & v)
{ w.writeData(reinterpret_cast<const uint8_t*>(&v), sizeof(v)); }

template <>
inline void Stream::Serializer<uint32_t>::read(Stream::ReaderIf & r, uint32_t & v)
{ r.readData(reinterpret_cast<uint8_t*>(&v), sizeof(v)); }




}}




template <typename T>
inline Flat::Utils::Stream::Writer & operator<<(Flat::Utils::Stream::Writer & w, const T & v)
{ w->write<T>(v); return w; }




#define FLAT_UTILS_STREAM_DECLARE_ENUM(Enum, Type) \
	namespace Flat { namespace Utils { \
	static_assert(std::is_enum_v<Enum>); \
	template <> struct Stream::EnumTraits<Enum> { \
		typedef Type I; \
	}; \
	}}

#define FLAT_UTILS_STREAM_DECLARE_TYPE(T) \
	namespace Flat { namespace Utils { \
	template <> struct Stream::TypeTraits<T> { inline static constexpr bool valid = true; }; \
	template <> void Stream::Serializer<T>::write(Stream::WriterIf &, const T &); \
	template <> void Stream::Serializer<T>::read(Stream::ReaderIf &, T &); \
	}}
