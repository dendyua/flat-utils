
#pragma once

#include <functional>

#include <QString>




class QSettings;




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT QtSettingsGroup
{
public:
	enum class Mode {
		None,
		Group,
		Array,
	};

	QtSettingsGroup(QSettings & settings);
	~QtSettingsGroup();

	QtSettingsGroup(const QtSettingsGroup &) = delete;
	void operator=(const QtSettingsGroup &) = delete;

	QSettings & settings() noexcept { return settings_; }

	int size() const noexcept;

	QtSettingsGroup cd(const QString & name);
	QtSettingsGroup readArray(const QString & name);
	QtSettingsGroup writeArray(const QString & name, int size);

private:
	QtSettingsGroup(QSettings & settings, Mode mode, int size);

	std::reference_wrapper<QSettings> settings_;

	Mode mode_ = Mode::None;
	int size_;
};




}}
