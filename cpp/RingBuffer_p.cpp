
#include "RingBuffer_p.hpp"

#include <algorithm>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




constexpr int kReserveCount = 65536;




RingBufferPrivate::RingBufferPrivate()
{
}


bool RingBufferPrivate::seek(const int pos)
{
	FLAT_ASSERT(pos >= 0 && pos <= size_);

	pos_ = pos;

	if (size_ == 0) {
		FLAT_ASSERT(pos_ == 0);
		ringBufferNumber_ = -1;
	} else {
		ringBufferPos_ = pos_ + offset_;
		for (ringBufferNumber_ = 0; ; ++ringBufferNumber_) {
			const int ringBufferSize = ring_.at(ringBufferNumber_)->size();

			if (ringBufferPos_ < ringBufferSize) break;

			ringBufferPos_ -= ringBufferSize;

			if (ringBufferPos_ == 0) {
				// seek was to the end
				FLAT_ASSERT(pos_ == size_);
				ringBufferNumber_ = -1;
				break;
			}
		}
	}

	return true;
}


int RingBufferPrivate::readData(uint8_t * const data, const int maxLen)
{
	FLAT_ASSERT(maxLen >= 0);

	if (ringBufferNumber_ == -1 || maxLen == 0 || pos_ == size_) return 0;

	int bytesRead = 0;

	uint8_t * p = data;
	int bytesRemain = maxLen;
	while (ringBufferNumber_ != -1 && bytesRemain != 0) {
		const Buffer & buffer = ring_.at(ringBufferNumber_);
		const int bufferSize = buffer->size();

		FLAT_ASSERT(ringBufferPos_ < bufferSize);

		const int bytesToCopy = std::min<int>(bytesRemain, bufferSize - ringBufferPos_);
		std::memcpy(p, buffer->data() + ringBufferPos_, static_cast<std::size_t>(bytesToCopy));

		p += bytesToCopy;
		bytesRead += bytesToCopy;
		bytesRemain -= bytesToCopy;

		// seek forward
		pos_ += bytesToCopy;
		ringBufferPos_ += bytesToCopy;
		if (ringBufferPos_ == bufferSize) {
			ringBufferNumber_++;
			ringBufferPos_ = 0;

			if (ringBufferNumber_ == int(ring_.size())) {
				ringBufferNumber_ = -1;
				// assert that we at end
				FLAT_ASSERT(pos_ == size_);
			}
		}
	}

	return bytesRead;
}


int RingBufferPrivate::writeData(const uint8_t * const data, const int maxLen)
{
	push(data, maxLen);
	return maxLen;
}


void RingBufferPrivate::appendBuffer(const int size)
{
	const auto buffer = [&] () -> Buffer {
		if (!cache_.empty()) {
			// take buffer from cache
			const Buffer buffer = cache_.back();
			buffer->resize(0);
			cache_.pop_back();
			return buffer;
		}

		// create new buffer
		const Buffer buffer = std::make_shared<std::vector<uint8_t>>();
		buffer->reserve(std::max(size, kReserveCount));
		return buffer;
	}();

	ring_.push_back(buffer);
}


void RingBufferPrivate::push(const uint8_t * const data, const int size)
{
	FLAT_ASSERT(size >= 0);

	if (size == 0) return;

	const uint8_t * p = data;
	int bytesRemain = size;
	while (bytesRemain > 0) {
		if (ring_.empty()) {
			appendBuffer(bytesRemain);
		} else {
			const Buffer & buffer = ring_.back();
			if (buffer->size() == buffer->capacity()) {
				appendBuffer(bytesRemain);
			}
		}

		Buffer & buffer = ring_.back();

		const int bytesToCopy = std::min<int>(buffer->capacity() - buffer->size(), bytesRemain);

		const int writePos = buffer->size();
		buffer->resize(writePos + bytesToCopy);
		std::memcpy(buffer->data() + writePos, p, static_cast<std::size_t>(bytesToCopy));

		p += bytesToCopy;
		bytesRemain -= bytesToCopy;
	}

	size_ += size;

	if (ringBufferNumber_ == -1) ringBufferNumber_ = 0;
}


void RingBufferPrivate::push(const Buffer & data)
{
	if (data->empty()) return;

	ring_.push_back(data);

	size_ += data->size();

	if (ringBufferNumber_ == -1) ringBufferNumber_ = 0;
}


void RingBufferPrivate::pop(const int size)
{
	FLAT_ASSERT(size <= size_);

	int bytesRemain = size;
	while (bytesRemain > 0) {
		Buffer & buffer = ring_.front();

		const int bytesToCut = std::min<int>(bytesRemain, buffer->size() - offset_);
		FLAT_ASSERT(bytesToCut > 0);

		if (bytesToCut == int(buffer->size()) - offset_) {
			cache_.push_back(buffer);
			ring_.erase(ring_.begin());
			offset_ = 0;
		} else {
			offset_ += bytesToCut;
		}

		bytesRemain -= bytesToCut;
	}

	size_ -= size;

	seek(0);
}


void RingBufferPrivate::clear()
{
	size_ = 0;
	offset_ = 0;

	// move buffers to cache
	cache_.insert(cache_.end(), ring_.cbegin(), ring_.cend());

	ring_.clear();

	seek(0);
}


void RingBufferPrivate::visit(const Visitor & visitor)
{
	if (size_ == 0) return;

	int sizeLeft = size_;
	for (int i = ringBufferNumber_; i < int(ring_.size()); ++i) {
		FLAT_ASSERT(sizeLeft > 0);
		const Buffer & buffer = ring_.at(i);
		const int offset = i == ringBufferNumber_ ? offset_ : 0;
		const int size = std::min(sizeLeft, int(buffer->size()) - offset);
		sizeLeft -= size;
		if (!visitor(buffer->data() + offset, size)) {
			break;
		}
	}
}




}}
