
#pragma once

#include <QMetaObject>




namespace Flat {
namespace Utils {




class FLAT_UTILS_QT_EXPORT QtConnection
{
public:
	QtConnection() noexcept {}
	QtConnection(const QMetaObject::Connection & c);
	QtConnection(QtConnection && other) noexcept : c_(std::move(other.c_)) {}
	QtConnection & operator=(QtConnection && other) noexcept { c_ = std::move(other.c_); return *this; }
	~QtConnection() noexcept;

	QtConnection(const QtConnection &) = delete;
	void operator=(const QtConnection &) = delete;

private:
	QMetaObject::Connection c_;
};




}}
