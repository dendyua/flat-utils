
#pragma once

#include <memory>
#include <vector>

#include <QIODevice>

#include <Flat/Core/Ref.hpp>




namespace Flat {
namespace Utils {




class FLAT_UTILS_QT_EXPORT QtRingBuffer : public QIODevice
{
	Q_OBJECT

public:
	QtRingBuffer(QObject * parent = 0);
	~QtRingBuffer();

	void push(const quint8 * data, int size);
	void push(const std::shared_ptr<std::vector<uint8_t>> & data);
	void pop(int size);
	void clear();

	// reimplemented from QIODevice
	bool open(QIODevice::OpenMode openMode) override;

	bool isSequential() const override;
	qint64 bytesAvailable() const override;
	qint64 size() const override;

	bool seek(qint64 pos) override;

protected:
	qint64 readData(char * data, qint64 maxLen) override;
	qint64 writeData(const char * data, qint64 maxLen) override;

private:
	struct D;

	Ref<D> d_;
};




}}
