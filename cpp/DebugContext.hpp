
#pragma once

#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(FlatUtils, FLAT_UTILS_SOURCE_DIR "/cpp", true)
FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(FlatUtils, 1, FLAT_UTILS_SOURCE_DIR "/include/Flat/Utils")
