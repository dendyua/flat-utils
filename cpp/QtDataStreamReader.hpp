
#pragma once

#include <QDataStream>




namespace Flat {
namespace Utils {




class FLAT_UTILS_QT_EXPORT DataStreamReader
{
public:
	DataStreamReader(QDataStream & ds);

	qint8 readInt8();
	quint8 readUint8();

	qint16 readInt16();
	quint16 readUint16();

	qint32 readInt32();
	quint32 readUint32();

	qint64 readInt64();
	quint64 readUint64();

	template <typename T> T read();

private:
	QDataStream & ds_;
};




inline DataStreamReader::DataStreamReader(QDataStream & ds) :
	ds_(ds)
{}

inline qint8 DataStreamReader::readInt8()
{ qint8 i; ds_ >> i; return i; }

inline quint8 DataStreamReader::readUint8()
{ quint8 i; ds_ >> i; return i; }

inline qint16 DataStreamReader::readInt16()
{ qint16 i; ds_ >> i; return i; }

inline quint16 DataStreamReader::readUint16()
{ quint16 i; ds_ >> i; return i; }

inline qint32 DataStreamReader::readInt32()
{ qint32 i; ds_ >> i; return i; }

inline quint32 DataStreamReader::readUint32()
{ quint32 i; ds_ >> i; return i; }

inline qint64 DataStreamReader::readInt64()
{ qint64 i; ds_ >> i; return i; }

inline quint64 DataStreamReader::readUint64()
{ quint64 i; ds_ >> i; return i; }

template <typename T>
inline T DataStreamReader::read()
{ T i; ds_ >> i; return i; }




}}
