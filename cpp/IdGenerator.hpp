
#pragma once

#include <memory>
#include <vector>

#include "UniqueData.hpp"




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT IdGenerator
{
public:
	inline static constexpr int kInvalidId = -1;

	class IdData
	{
	public:
		IdData() noexcept {}
		int id() const noexcept { return id_; }

	protected:
		void destroy() noexcept { g_->free(id_); }

	private:
		IdData(IdGenerator & g) noexcept : g_(&g) { id_ = g_->take(); }
		IdData(IdGenerator & g, int id) noexcept : g_(&g), id_(id) {}

	private:
		IdGenerator * g_;
		int id_;

		friend class IdGenerator;
	};

	typedef UniqueData<IdData> Id;

	class Iterator
	{
	public:
		Iterator(const IdGenerator & idGenerator) noexcept;

		bool hasNext() const noexcept;
		int next() noexcept;
		int current() const noexcept;

	private:
		const IdGenerator & idGenerator_;
		int currentId_;
		int nextId_;
	};


	IdGenerator(int limit = -1) noexcept;

	int limit() const noexcept;

	Id takeUnique() noexcept;
	Id adapt(int id) noexcept;

	int take() noexcept;
	void free(int id) noexcept;
	void clear() noexcept;
	void reserve(int id) noexcept;
	bool isFree(int id) const noexcept;
	bool isTaken(int id) const noexcept;

	bool isEmpty() const noexcept;
	bool isFull() const noexcept;
	int count() const noexcept;

	int first() const noexcept;

	struct iterator_t {
		iterator_t(const IdGenerator & g, int cur) noexcept : g(g), cur(cur) {}
		void operator=(const iterator_t & it) noexcept { cur = it.cur; }

		const IdGenerator & g;
		int cur;
		int operator*() const noexcept { return cur; }
		bool operator==(const iterator_t & o) const noexcept { return cur == o.cur; }
		bool operator!=(const iterator_t & o) const noexcept { return cur != o.cur; }
		void operator++() noexcept { cur = g.ids_.at(cur).next; }
	};

	iterator_t begin() const noexcept { return iterator_t(*this, firstTaken_); }
	iterator_t end() const noexcept { return iterator_t(*this, kInvalidId); }
	iterator_t erase(const iterator_t & it) noexcept;

private:
	struct IdInfo
	{
		int next;
		int previous;
		bool isFree;
	};

private:
	void _makeIdTaken(int id, IdInfo * idp) noexcept;

private:
	int limit_;
	int firstFree_;
	int lastFree_;
	int firstTaken_;
	int lastTaken_;
	int freeCount_;
	std::vector<IdInfo> ids_;
};




inline IdGenerator::Iterator::Iterator(const IdGenerator & idGenerator) noexcept :
	idGenerator_(idGenerator),
	currentId_(IdGenerator::kInvalidId),
	nextId_(idGenerator.firstTaken_)
{}

inline bool IdGenerator::Iterator::hasNext() const noexcept
{ return nextId_ != kInvalidId; }

inline int IdGenerator::Iterator::next() noexcept
{
	currentId_ = nextId_;
	nextId_ = idGenerator_.ids_.at(currentId_).next;
	return currentId_;
}

inline int IdGenerator::Iterator::current() const noexcept
{ return currentId_; }




inline int IdGenerator::count() const noexcept
{ return static_cast<int>(ids_.size()) - freeCount_; }

inline int IdGenerator::limit() const noexcept
{ return limit_; }

inline bool IdGenerator::isEmpty() const noexcept
{ return freeCount_ == static_cast<int>(ids_.size()); }

inline bool IdGenerator::isTaken(const int id) const noexcept
{ return !isFree(id); }

inline bool IdGenerator::isFull() const noexcept
{ return count() == limit(); }

inline int IdGenerator::first() const noexcept
{ return firstTaken_; }




}}
