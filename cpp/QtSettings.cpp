
#include "QtSettings.hpp"

#include <QSettings>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




QtSettingsGroup::QtSettingsGroup(QSettings & settings, const Mode mode, const int size) :
	settings_(settings),
	mode_(mode),
	size_(size)
{
}


QtSettingsGroup::QtSettingsGroup(QSettings & settings) :
	settings_(settings)
{
}


QtSettingsGroup::~QtSettingsGroup()
{
	switch (mode_) {
	case Mode::None:
		break;

	case Mode::Group:
		settings_.get().endGroup();
		break;

	case Mode::Array:
		settings_.get().endArray();
		break;
	}
}


int QtSettingsGroup::size() const noexcept
{
	FLAT_ASSERT(mode_ == Mode::Array);
	return size_;
}


QtSettingsGroup QtSettingsGroup::cd(const QString & group)
{
	settings_.get().beginGroup(group);
	return QtSettingsGroup(settings_.get(), Mode::Group, -1);
}


QtSettingsGroup QtSettingsGroup::readArray(const QString & name)
{
	const int size = settings_.get().beginReadArray(name);
	return QtSettingsGroup(settings_.get(), Mode::Array, size);
}


QtSettingsGroup QtSettingsGroup::writeArray(const QString & name, const int size)
{
	settings_.get().beginWriteArray(name, size);
	return QtSettingsGroup(settings_.get(), Mode::Array, size);
}




}}
