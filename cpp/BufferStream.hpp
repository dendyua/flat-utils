
#pragma once

#include <functional>

#include "Stream.hpp"




namespace Flat {
namespace Utils {




class FLAT_UTILS_EXPORT BufferStream : public Stream
{
public:
	typedef std::function<bool(const uint8_t*, int)> Visitor;

	BufferStream();
	~BufferStream() override;

	void clear();
	void visit(const Visitor & visitor);

protected:
	int size() const override;
	int pos() const override;
	void seek(int pos) override;
	int bytesAvailable() const override;
	int readData(uint8_t * data, int size) override;
	void commit(int pos) override;
	int writeData(const uint8_t * data, int size) override;

private:
	struct D;

	const D & _d() const noexcept;
	D & _d() noexcept;
};




}}
