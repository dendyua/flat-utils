
#include "QtConnection.hpp"

#include <QObject>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




QtConnection::QtConnection(const QMetaObject::Connection & c) :
	c_(c)
{
	FLAT_CHECK(c_);
}


QtConnection::~QtConnection() noexcept
{
	if (c_) {
		QObject::disconnect(c_);
	}
}




}}
