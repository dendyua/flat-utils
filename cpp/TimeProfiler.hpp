
#pragma once

#include <algorithm>
#include <array>
#include <chrono>
#include <cstdint>
#include <cstring>
#include <vector>

#include "Debug.hpp"




namespace Flat {
namespace Utils {




template <typename Id, int Size>
class TimeProfiler
{
public:
	TimeProfiler();

	void reset();

	void start(Id id);
	void stop();
	void restart(Id id);

	int64_t value(Id id) const;

private:
	struct ValueInfo {
		int64_t usecs;
	};

	struct Element {
		Id id;
		std::chrono::steady_clock::time_point time;
		bool valid;
	};

	void _push(Id id, const std::chrono::steady_clock::time_point & time);
	void _pop(const std::chrono::steady_clock::time_point & time);

	std::array<ValueInfo, Size> values_;

	std::vector<Element> stack_;
	int stackSize_ = 0;
};




template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::_push(const Id id,
		const std::chrono::steady_clock::time_point & time)
{
	const bool idExists = std::any_of(stack_.begin(), stack_.end(), [id] (const Element & e) {
		return e.id == id;
	});

	stack_.emplace_back(Element{id, time, !idExists});
}


template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::_pop(const std::chrono::steady_clock::time_point & time)
{
	FLAT_ASSERT(!stack_.empty());
	const Element & e = stack_.back();
	if (e.valid) {
		const int64_t usecs = std::chrono::duration_cast<std::chrono::microseconds>(
				time - e.time).count();
		values_[static_cast<size_t>(e.id)].usecs += usecs;
	}
}


template <typename Id, int Size>
inline TimeProfiler<Id, Size>::TimeProfiler()
{
}


template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::reset()
{
	std::memset(values_.data(), 0, sizeof(ValueInfo) * values_.size());
}


template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::start(const Id id)
{
	_push(id, std::chrono::steady_clock::now());
}


template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::stop()
{
	_pop(std::chrono::steady_clock::now());
}


template <typename Id, int Size>
inline void TimeProfiler<Id, Size>::restart(const Id id)
{
	const auto now = std::chrono::steady_clock::now();
	_pop(now);
	_push(id, now);
}


template <typename Id, int Size>
inline int64_t TimeProfiler<Id, Size>::value(const Id id) const
{
	return values_[size_t(id)].usecs;
}




}}
