
#include "Stream.hpp"

#include "Debug.hpp"
#include "Stream_p.hpp"




namespace Flat {
namespace Utils {




int Stream::WriterIf::size() const
{
	const Stream & stream = reinterpret_cast<const Stream&>(*this);
	return stream.size();
}


void Stream::WriterIf::writeData(const uint8_t * const data, const int size)
{
	FLAT_ASSERT(size >= 0);
	if (size == 0) return;

	Stream & stream = reinterpret_cast<Stream&>(*this);

	const int sizeWritten = stream.writeData(data, size);
	FLAT_ASSERT(sizeWritten >= 0);

	if (sizeWritten != size) {
		throw Stream::WriteError{};
	}

	stream.d_->hasBytesWritten = true;
}




int Stream::ReaderIf::pos() const
{
	const Stream & stream = reinterpret_cast<const Stream&>(*this);
	return stream.pos();
}


int Stream::ReaderIf::bytesAvailable() const
{
	const Stream & stream = reinterpret_cast<const Stream&>(*this);
	return stream.bytesAvailable();
}


void Stream::ReaderIf::readData(uint8_t * const data, const int size)
{
	FLAT_ASSERT(size >= 0);
	if (size == 0) return;

	Stream & stream = reinterpret_cast<Stream&>(*this);

	const int sizeRead = stream.readData(data, size);
	FLAT_ASSERT(sizeRead >= 0);

	if (sizeRead != size) {
		throw IncompleteError{};
	}
}


void Stream::ReaderIf::commit()
{
	Stream & stream = reinterpret_cast<Stream&>(*this);
	stream.d_->posRead = stream.pos();
}




void Stream::WriterDeleter::operator()(WriterIf * const w)
{
	Stream & stream = reinterpret_cast<Stream&>(*w);
	FLAT_ASSERT(stream.d_->writerLocked);
	stream.d_->writerLocked = false;

	if (stream.d_->hasBytesWritten) {
		stream.d_->hasBytesWritten = false;

		if (stream.d_->readerCallbacks) {
			try {
				stream.d_->readerCallbacks->ready();
			} catch (const IncompleteError &) {
			}
		}
	}
}


void Stream::ReaderDeleter::operator()(ReaderIf * const r)
{
	Stream & stream = reinterpret_cast<Stream&>(*r);
	FLAT_ASSERT(stream.d_->readerLocked);
	stream.d_->readerLocked = false;

	if (stream.d_->posRead == 0) {
		return;
	}

	stream.commit(stream.d_->posRead);

	if (stream.d_->writerCallbacks) {
		stream.d_->writerCallbacks->ready();
	}
}




Stream::Stream() :
	d_(*new D)
{
}


Stream::Stream(D & d) :
	d_(d)
{
}


Stream::~Stream()
{
}


void Stream::setReaderCallbacks(ReaderCallbacks * const cb)
{
	d_->readerCallbacks = cb;
}


void Stream::setWriterCallbacks(WriterCallbacks * const cb)
{
	d_->writerCallbacks = cb;
}


Stream::Reader Stream::lockForRead()
{
	FLAT_ASSERT(!d_->readerLocked);
	d_->readerLocked = true;
	d_->posRead = 0;
	return Reader(reinterpret_cast<ReaderIf*>(this), ReaderDeleter{});
}


Stream::Writer Stream::lockForWrite()
{
	FLAT_ASSERT(!d_->writerLocked);
	d_->writerLocked = true;
	return Writer(reinterpret_cast<WriterIf*>(this), WriterDeleter{});
}




}}
