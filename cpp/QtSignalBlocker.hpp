
#pragma once

#include <QPointer>
#include <QObject>




namespace Flat {
namespace Utils {




class QtSignalBlocker
{
public:
	QtSignalBlocker(QObject & object) noexcept :
		wasBlocked_(object.blockSignals(true)),
		pointer_(&object)
	{
	}

	~QtSignalBlocker() noexcept
	{
		if (pointer_) {
			if (!wasBlocked_) {
				pointer_->blockSignals(false);
			}
		}
	}

private:
	const bool wasBlocked_;
	const QPointer<QObject> pointer_;
};




}}
