
#pragma once

#include "Debug.hpp"




namespace Flat {
namespace Utils {




template <typename T>
class UniqueData : private T
{
public:
	UniqueData() :
		isNull_(true)
	{}

	UniqueData(const T & data) :
		T(data),
		isNull_(false)
	{}

	UniqueData(UniqueData && other) :
		T(static_cast<const T&>(other)),
		isNull_(other.isNull_)
	{
		other.isNull_ = true;
	}

	UniqueData & operator=(UniqueData && other)
	{
		_clear();
		_move(other);
		return *this;
	}

	~UniqueData()
	{
		_clear();
	}

	bool isNull() const
	{
		return isNull_;
	}

	void reset()
	{
		_clear();
	}

	void reset(const T & other)
	{
		_clear();
		T::operator=(other);
		isNull_ = false;
	}

	const T & release()
	{
		isNull_ = true;
		return static_cast<const T&>(*this);
	}

	const T * operator->() const
	{
		return static_cast<const T*>(this);
	}

	T * operator->()
	{
		return static_cast<T*>(this);
	}

	const T & operator*() const
	{
		return static_cast<const T&>(*this);
	}

	T & operator*()
	{
		return static_cast<T&>(*this);
	}

private:
	void _clear()
	{
		if ( !isNull_ ) {
			T::destroy();
			isNull_ = true;
		}
	}

	void _move(UniqueData<T> & other)
	{
		FLAT_ASSERT(isNull_);
		static_cast<T&>(*this) = static_cast<const T&>(other);
		isNull_ = other.isNull_;
		other.isNull_ = true;
	}

	UniqueData(const UniqueData<T> &) = delete;
	UniqueData<T> & operator=(const UniqueData<T> &) = delete;

private:
	bool isNull_;
};




}}
