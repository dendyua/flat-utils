
#include "QtRingBuffer.hpp"

#include "Debug.hpp"
#include "RingBuffer_p.hpp"




namespace Flat {
namespace Utils {




struct QtRingBuffer::D : public RingBufferPrivate {};




/**
 * \class QtRingBuffer
 *
 * \ingroup tools_module
 *
 * \brief The QtRingBuffer class represents FIFO buffer with QIODevice interface.
 *
 * Ring buffer is handy, when you have sequential FIFO stream and should collect
 * bytes from it until solid block of useful data to extract will arrive.
 *
 * Such typical stream is a TCP socket, that gives you bytes portion by portion
 * and you can't know when transmitted solid packet will arrive until you parse it fully.
 *
 * In this case you can simply put received bytes into ring buffer and try to extract something from it.
 * If packet still is not solid - wait for more bytes, append it to ring buffer and try again.
 *
 * QtRingBuffer cares about memory allocation, and never rearranges internal data chunks while reading
 * data by small pieces.
 *
 * Example of typical usage:
 *
 * \code
 * void MyNetworkHandler::readyRead()
 * {
 *     // appending all received bytes
 *     ringBuffer_.push(tcpSocket_->readAll());
 *
 *     // opening buffer for reading
 *     ringBuffer_.open(QIODevice::ReadOnly);
 *     QDataStream stream(&ringBuffer_);
 *
 *     // for example we are waiting a solid block of 'int', followed by 'QString'
 *     int myInt;
 *     QString myString;
 *     stream >> myInt;
 *     stream >> myString;
 *
 *     // check if our block is solid
 *     if (stream.status() == QDataStream::ReadPastEnd) {
 *         // not enough bytes in the stream, will wait for the next readyRead() call
 *         ringBuffer_.close();
 *         return;
 *     }
 *
 *     // check that our block encountered error, because of received junk from the network
 *     if (stream.status() == QDataStream::ReadCorruptData) {
 *         ringBuffer_.close();
 *         tcpSocket_->close();
 *         return;
 *     }
 *
 *     // here myInt and myString variables are valid, we can process them
 *     qDebug() << "Received from network:" << myInt << myString;
 *
 *     // and remove handled bytes from ring buffer
 *     const qint64 wellReadPos = ringBuffer_.pos();
 *     ringBuffer_.close();
 *     ringBuffer_.pop(wellReadPos);
 * }
 * \endcode
 */


/**
 * Constructs empty ring buffer.
 * \a parent is passed to the QObject constructor.
 */

QtRingBuffer::QtRingBuffer(QObject * const parent) :
	QIODevice(parent),
	d_(*new D())
{
}


/**
 * Destroys the ring buffer.
 */

QtRingBuffer::~QtRingBuffer()
{
}


/**
 * Appends the given \a data bytes of the \a size to the buffer.
 * Note that buffer must be closed.
 *
 * \sa pop()
 */

void QtRingBuffer::push(const quint8 * const data, const int size)
{
	FLAT_ASSERT(!isOpen());

	d_->push(data, size);
}


/**
 * \overload
 *
 * Appends the given \a data byte array to the buffer.
 * Note that buffer must be closed.
 *
 * \sa pop()
 */

void QtRingBuffer::push(const std::shared_ptr<std::vector<uint8_t>> & data)
{
	FLAT_ASSERT(!isOpen());

	d_->push(data);
}


/**
 * Removes \a size bytes from the start of the buffer.
 * Note that buffer must be closed.
 *
 * \sa push()
 */

void QtRingBuffer::pop(const int size)
{
	FLAT_ASSERT(!isOpen());

	d_->pop(size);
}


/**
 * Clears the buffer. Equivalent of pop(size()).
 * Note that buffer must be closed.
 */

void QtRingBuffer::clear()
{
	FLAT_ASSERT(!isOpen());

	d_->clear();
}


/** \internal
 */

bool QtRingBuffer::open(const QIODevice::OpenMode openMode)
{
	if (openMode == QIODevice::ReadOnly) {
		d_->seek(0);
	} else if ((openMode & QIODevice::WriteOnly) && (openMode & QIODevice::Append)) {
	} else {
		FLAT_FATAL << "Only (ReadOnly) and (WriteOnly|Append) modes are allowed.";
	}

	return QIODevice::open(openMode);
}


/** \internal
 */

bool QtRingBuffer::isSequential() const
{
	return false;
}


/** \internal
 */

qint64 QtRingBuffer::bytesAvailable() const
{
	return d_->size() - d_->pos() + QIODevice::bytesAvailable();
}


/** \internal
 */

qint64 QtRingBuffer::size() const
{
	return d_->size();
}


/** \internal
 */

bool QtRingBuffer::seek(const qint64 pos)
{
	QIODevice::seek(pos);
	return d_->seek(static_cast<int>(pos));
}


/** \internal
 */

qint64 QtRingBuffer::readData(char * const data, const qint64 maxLen)
{
	return d_->readData(reinterpret_cast<quint8*>(data), static_cast<int>(maxLen));
}


/** \internal
 */

qint64 QtRingBuffer::writeData(const char * const data, const qint64 maxLen)
{
	return d_->writeData(reinterpret_cast<const quint8*>(data), static_cast<int>(maxLen));
}




}}
